/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ioctl.h>
#include <simpleprint/simpleprint.h>

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
const char day2char[][4] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
const char mon2char[][4] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
	"Aug", "Sep", "Oct", "Nov", "Dec"};

///////////////////////////////////////////////////////////////////////////////
// Utility
///////////////////////////////////////////////////////////////////////////////
char *CheckLength (char *s)
{
	// If there is only one decimal, prepend a '0'
	if (!s[1])
	{
		s[2] = s[1];
		s[1] = s[0];
		s[0] = '0';
	}

	return s;
}

///////////////////////////////////////////////////////////////////////////////
// Get and set time using the driver
///////////////////////////////////////////////////////////////////////////////
int GetTime (const char *filename, struct tm *rtcvalue)
{
	int fd = open (filename, O_RDONLY);

	if (fd < 0)
		return fd;

	int ret = ioctl (fd, RTCGETTIME, rtcvalue);

	close (fd);
	return ret;
}

int SetTime (const char *filename, struct tm *rtcvalue)
{
	int fd = open (filename, O_RDWR);

	if (fd < 0)
		return fd;

	int ret = ioctl (fd, RTCSETTIME, rtcvalue);

	close (fd);
	return ret;
}

///////////////////////////////////////////////////////////////////////////////
// Check that a time structure is valid
///////////////////////////////////////////////////////////////////////////////
int CheckTimeStruct (struct tm *t)
{
	// Check weekday
	if (t->tm_wday < 0 || t->tm_wday > 6)
		return 1;

	// Check month
	if (t->tm_mon < 0 || t->tm_mon > 11)
		return 1;

	// Check day
	if (t->tm_mday < 1 || t->tm_mday > 31)
		return 1;

	// Check hours
	if (t->tm_hour < 0 || t->tm_hour > 23)
		return 1;

	// Check minutes
	if (t->tm_min < 0 || t->tm_min > 59)
		return 1;

	// Check seconds (60 because leap seconds)
	if (t->tm_sec < 0 || t->tm_sec > 60)
		return 1;

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Output printing of time in different formats
///////////////////////////////////////////////////////////////////////////////
void PrintTimeStruct (struct tm *rtcvalue)
{
	PrintValue ("sec ", rtcvalue->tm_sec);
	PrintValue ("min ", rtcvalue->tm_min);
	PrintValue ("hour", rtcvalue->tm_hour);
	PrintValue ("mday", rtcvalue->tm_mday);
	PrintValue ("mon ", rtcvalue->tm_mon);
	PrintValue ("year", rtcvalue->tm_year);
	PrintValue ("wday", rtcvalue->tm_wday);
	PrintValue ("yday", rtcvalue->tm_yday);
	PrintValue ("isdst", rtcvalue->tm_isdst);
}

void PrintTimeRFC2822 (struct tm *t)
{
	// Check
	if (CheckTimeStruct (t))
		return;

	// Print weekday
	SimplePrint ((char*) day2char[t->tm_wday]);
	SimplePrint (" ");

	// Print day in month
	char str[16];
	_itoa (t->tm_mday, str);
	SimplePrint (str);
	SimplePrint (" ");

	// Print month
	SimplePrint ((char*) mon2char[t->tm_mon]);
	SimplePrint (" ");

	// Print year
	_itoa (t->tm_year + 1900, str);
	SimplePrint (str);
	SimplePrint (" ");

	// Print hours, minutes and seconds
	_itoa (t->tm_hour, str);
	SimplePrint (CheckLength(str));
	SimplePrint (":");
	_itoa (t->tm_min, str);
	SimplePrint (CheckLength(str));
	SimplePrint (":");
	_itoa (t->tm_sec, str);
	SimplePrint (CheckLength(str));
	SimplePrint (" ");

	// Print time zone
	//SimplePrint ("+0200\n");
	SimplePrint ("\n");
}

void PrintTimeUniversal (struct tm *t)
{
	// Check
	if (CheckTimeStruct (t))
		return;

	// Print weekday
	SimplePrint ((char*) day2char[t->tm_wday]);
	SimplePrint (" ");

	// Print month
	SimplePrint ((char*) mon2char[t->tm_mon]);
	SimplePrint (" ");

	// Print day in month
	char str[16];
	_itoa (t->tm_mday, str);
	SimplePrint (str);
	SimplePrint (" ");

	// Print hours, minutes and seconds
	_itoa (t->tm_hour, str);
	SimplePrint (CheckLength(str));
	SimplePrint (":");
	_itoa (t->tm_min, str);
	SimplePrint (CheckLength(str));
	SimplePrint (":");
	_itoa (t->tm_sec, str);
	SimplePrint (CheckLength(str));
	SimplePrint (" ");

	// Print time zone
	//SimplePrint ("+0200");

	// Print year
	_itoa (t->tm_year + 1900, str);
	SimplePrint (str);
	SimplePrint ("\n");
}

///////////////////////////////////////////////////////////////////////////////
// Convert char to int
///////////////////////////////////////////////////////////////////////////////
int Char2Day (char *s)
{
	for (int i = 0; i < 7; i++)
		if (!strcmp (s, day2char[i]))
			return i;

	return -1;
}

int Char2Mon (char *s)
{
	for (int i = 0; i < 12; i++)
		if (!strcmp (s, mon2char[i]))
			return i;

	return -1;
}

///////////////////////////////////////////////////////////////////////////////
// Set time
///////////////////////////////////////////////////////////////////////////////
int SetTimeRFC2822 (char **s)
{
	struct tm rtcvalue;
	char *tok;

	rtcvalue.tm_wday = Char2Day (s[0]);
	rtcvalue.tm_mday = atoi (s[1]);
	rtcvalue.tm_mon = Char2Mon (s[2]);
	rtcvalue.tm_year = atoi (s[3]) - 1900;

	tok = strtok (s[4], ":");
	rtcvalue.tm_hour = atoi (tok);

	tok = strtok (0, ":");
	rtcvalue.tm_min = atoi (tok);

	tok = strtok (0, ":");
	rtcvalue.tm_sec = atoi (tok);

	if (CheckTimeStruct (&rtcvalue))
	{
		SimplePrint ("The time specified is invalid\n");
		return 1;
	}

	return SetTime ("/dev/rtc", &rtcvalue);
}

///////////////////////////////////////////////////////////////////////////////
// Usage
///////////////////////////////////////////////////////////////////////////////
int PrintUsage ()
{
	SimplePrint ("Usage: date [option]\n"
		   "  --rfc-2822\n"
		   "  -R\n"
		   "    Print in RFC 2822 format\n"
		   "  -u\n"
		   "  --utc\n"
		   "  --universal\n"
		   "    Print in Coordinated\n"
		   "    Universal Time (UTC)\n"
		   "  -s [string]\n"
		   "    Set time using RFC 2822\n"
		   "  --help\n"
		   "    Print this message\n"
		   "  -d\n"
		   "    Print debug information\n");
	return 1;
}

int PrintUnknownCommand (void)
{
	SimplePrint ("Invalid argument, use\n   date --help\nfor a usage list\n");
	return 1;
}


///////////////////////////////////////////////////////////////////////////////
// Main function
///////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	// Variables
	struct tm rtcvalue;
	int ret;

	// Get time
	if ((ret = GetTime ("/dev/rtc", &rtcvalue)))
		return PrintError ("date", "/dev/rtc");

	// If no arguments, print default
	if (argc == 1)
	{
		PrintTimeRFC2822 (&rtcvalue);
		return 0;
	}

	// Check arguments
	if (argc == 2)
	{
		if (!strcmp (argv[1], "--rfc-2822") ||
			!strcmp (argv[1], "-R"))
			PrintTimeRFC2822 (&rtcvalue);
		else if (!strcmp (argv[1], "-u") ||
			!strcmp (argv[1], "--utc") ||
			!strcmp (argv[1], "--universal"))
			PrintTimeUniversal (&rtcvalue);
		else if (!strcmp(argv[1], "--help"))
			PrintUsage ();
		else if (!strcmp(argv[1], "-d"))
			PrintTimeStruct (&rtcvalue);
		else
			return PrintUnknownCommand ();

		// OK
		return 0;
	}
	else if (argc == (2+5) && !strcmp (argv[1], "-s"))
		return SetTimeRFC2822 (&argv[2]);

	// Error
	return PrintUnknownCommand ();
}
